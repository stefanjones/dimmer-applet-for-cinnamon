# Screen Dimmer Applet for Cinnamon #

Multi-monitor support for controlling the screen brightness using XRandR. This will dim the display without changing the backlight settings.

### Compatibility

* Built on Linux Mint 18 Cinnamon
* Requires Python 3
* Requires Cinnamon 1.8 or newer

### Installation

* Copy the bright@stefanjones.ca folder inside your /home/USERNAME/.local/share/cinnamon/applets folder
* Brightness can be controlled either by clicking on the taskbar icon, or by configuring the applet. 
* Configure the app to your desired brightness level. Changes will take effect immediately. 

### Additional Notes

* Brightness changes apply to all connected screens.
* If the desktop goes into power-saving mode, the screens may revert to normal brightness. Clicking the taskbar icon will re-apply your brightness setting.
* Tested on Intel graphics drivers. AMD and NVIDIA may perform differently.
