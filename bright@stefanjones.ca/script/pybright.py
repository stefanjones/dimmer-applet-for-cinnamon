#!/usr/bin/python3

import gi
import sys
gi.require_version('Gdk', '3.0')
from gi.repository import Gdk
import subprocess

desc = '''Python Brightness Control for XRandR 0.1
Stefan Jones - stefan.jones@usask.ca

Set the brightness level of all connected screens using X11.
Accepts one parameter, ranging from 0.1 (dim) to 1.0 (normal)

This will not affect backlight levels and is temporary until the next reboot.
Example: ./pybright.py 0.8'''

def main():

	# Accept a limited range on the passed-in argument
	try:
		bright=float(sys.argv[1])
	except (ValueError, IndexError):
		print(desc)
		sys.exit(1)

	if float(bright) > 1.0:
		print("Brightness value cannot be above 1.0")
		sys.exit(2)
	if float(bright) < 0.1:
		print("Brightness value cannot be below 0.1")
		sys.exit(2)

	screen=Gdk.Screen.get_default()

	# Call 'xrandr --output INTERFACENAME --brightness VALUE.2f' on each display
	for i in range(screen.get_n_monitors()):
		plug = screen.get_monitor_plug_name(i)
		subprocess.run(['xrandr','--output',plug,'--brightness',("%.2f" % bright)])

if __name__ == "__main__":
    main()
