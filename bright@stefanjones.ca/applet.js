const Interfaces = imports.misc.interfaces;
const Gdk = imports.gi.Gdk;
const PopupMenu = imports.ui.popupMenu;

const Applet = imports.ui.applet;
const Util = imports.misc.util;
const Settings = imports.ui.settings;

const Gio = imports.gi.Gio;
const Main = imports.ui.main;
const Lang = imports.lang;

function MyApplet(orientation, panel_height, instance_id) {
    this._init(orientation, panel_height, instance_id);
}

MyApplet.prototype = {
    __proto__: Applet.IconApplet.prototype,

    _init: function(orientation, panel_height, instance_id) {   
		Applet.IconApplet.prototype._init.call(this, orientation, panel_height, instance_id);

        this.menuManager = new PopupMenu.PopupMenuManager(this);
        this.menu = new Applet.AppletPopupMenu(this, orientation);
        this.menuManager.addMenu(this.menu);

        this.settings = new Settings.AppletSettings(this, "bright@stefanjones.ca", instance_id);
        this.settings.bindProperty(Settings.BindingDirection.IN,
                           "brightness",
                           "brightness",
                           this.on_settings_changed,
                           null);
		this.script_filepath = global.userdatadir+"/applets/bright@stefanjones.ca/script/pybright.py";
        this.set_applet_icon_name("dimmer");
        this.set_applet_tooltip(_("Apply screen brightness settings"));
        this.set_applet_icon_symbolic_name("preferences-desktop-wallpaper");

        this.slider = new PopupMenu.PopupSliderMenuItem(0);
        this.slider.connect("value-changed", Lang.bind(this, this.on_slider_changed));

        this.menu.addMenuItem(this.slider);
	},
    
    on_applet_clicked: function(event) {    
        this.slider.setValue(this.brightness);
        this.menu.toggle();
        Util.spawn([this.script_filepath, this.brightness.toString()]);
    },

    on_applet_removed_from_panel: function() {
        Util.spawn([this.script_filepath, "1.0"]);
    },

    on_settings_changed: function(event) {
        this.slider.setValue(this.brightness);
        Util.spawn([this.script_filepath, this.brightness.toString()]);
    },

    on_slider_changed: function(slider, value) {
        this.brightness = value;
        Util.spawn([this.script_filepath, this.brightness.toString()]);
    },

};

function main(metadata, orientation, panel_height, instance_id) {  
    let myApplet = new MyApplet(orientation, panel_height, instance_id);
    return myApplet;      
}


